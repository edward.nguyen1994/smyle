from django.db import models

# Create your models here.

class Question(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    post_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name