from django.shortcuts import render, redirect
from datetime import date
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import Post
from .forms import PostCreateForm
from questions.models import Question

today = date.today()
question_of_day = Question.objects.filter(post_date=today)

def list_posts(request):
    posts_of_day = Post.objects.filter(question=question_of_day.get())
    context = {
        "today": today,
        "posts_of_day": posts_of_day,
        "question_of_day": question_of_day,
        # .filter returns queryset, .get returns object (see posts/list.html)
    }
    return render(request, "posts/list.html", context)

@login_required
def create_post(request):
    if request.method == "POST":
        form = PostCreateForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.question = question_of_day.get()
            post.save()
            return redirect("home")
            # return redirect("detail_question", pk=post.question.id)
    else:
        form = PostCreateForm()
    context = {
        "form": form,
        "today": today,
        "question_of_day": question_of_day,
        }
    return render(
        request,
        "posts/create.html",
        context,
    )