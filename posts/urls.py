from django.urls import path

from .views import list_posts, create_post

urlpatterns = [
    path("", list_posts, name="list_posts"),
    path("create/", create_post, name="create_post"),
    ]