from django.urls import path
from django.contrib.auth import views as auth_views

from .views import signup_accounts

urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("signup/", signup_accounts, name="signup"),
    # path("account/", detail_account, name="account"),
]